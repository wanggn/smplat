<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
  <!-- 引入标签库 -->
	<jsp:include page="/common/meta.jsp"/>
    
    <title>easyui</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
  </head>
  <script type="text/javascript">
   $(function(){
   	$("#tabs").tabs('add',{
   		title:'首页',
   	});
   });
  
  </script>
  <body class="easyui-layout">
	<div data-options="region:'north'" style="height: 50px;">
		<!-- 页面顶部 上北下南 左西右东的分布 -->
	</div>
	<div data-options="region:'west',title:'主菜单'" style="width: 200px;">
		<!-- 页面左部 西边 显示菜单 -->
		<div class="easyui-accordion" data-options="border:false" id='menu'>
			${menus}</div>
	</div>
	<div data-options="region:'center'">
		<div id='tabs' class="easyui-tabs"
			data-options="fit:true,border:false"></div>
	</div>
	<div data-options="region:'south',noheader:true,split:false"
		style="height:20px;">
			<center>2017 技术有限 只能这样</center>
		</div>
</body>
</html>
