package com.xiaoshuai.plat.controller.system.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import sun.net.www.content.text.plain;

import com.xiaoshuai.plat.base.BaseDao;
import com.xiaoshuai.plat.controller.system.service.MenuService;
import com.xiaoshuai.plat.pojo.Menu;
/**
 * 菜单实现类
 * @author 小帅帅丶
 * @Title MenuServiceImpl
 * @时间   2017-2-8下午3:01:36
 */
@Service("menuService")
public class MenuServiceImpl implements MenuService {
	@Autowired
	private BaseDao baseDao;
	/**
	 * @param name 
	 * @param start 开始从X读取
	 * @param size  每页大小
	 * @param order 排序字段
	 */
	@Override
	public String list(String name, int start, int size, String order) {
		String sql = "select * from menu m where 1=1 ";
		String sqlfooter = "select sum(num) as num,'总和' as icon from menu where 1=1";
		List<Object> param = new ArrayList<Object>();
		if(name!=null && name.length()>0){
			sql += " and m.menuname like ? or m.icon like ? or m.url like ?";
			param.add("%"+name+"%");
			param.add("%"+name+"%");
			param.add("%"+name+"%");
		}	
		List<Map<String, Object>> list = baseDao.listByNative(sql, param.toArray(), start, size, order);
//		List<Map<String, Object>> footer = baseDao.listByNative(sqlfooter, param.toArray(), start, size, order);
		int count = count(name,start, size, order);
		Map<String,Object> map = new HashMap<String, Object>();
		map.put("total", count);
		map.put("rows", list);
//		map.put("footer",footer);
		JSONObject object = JSONObject.fromObject(map);
		String result = object.toString();
		return result;
	}
	/**
	 * 根据条件统计
	 * @param name
	 * @param start
	 * @param size
	 * @param order
	 * @return
	 */
	@Override
	public int count(String name, int start, int size, String order) {
		String sql = "select count(*) from menu m where 1=1 ";
		List<Object> param = new ArrayList<Object>();
		if(name!=null && name.length()>0){
			sql += " and m.menuname like ? or m.icon like ? or m.url like ?";
			param.add("%"+name+"%");
			param.add("%"+name+"%");
			param.add("%"+name+"%");
		}	
		int count = baseDao.countByNative(sql, param.toArray());
		return count;
	}
	/**
	 * 修改方法
	 */
	@Override
	public String update(HttpServletRequest request,Menu menu, String id) {
		Menu menus = baseDao.get(Menu.class, id);
		if(null!=menu){
			
		}
		return null;
	}
	/**
	 * 根据id查询 并返回对象
	 */
	@Override
	public Menu getMenuById(Integer id) {
		Menu menus = new Menu();
		String sql = "select * from menu where id="+id;
		try {
			 menus = baseDao.findByOne(sql);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return menus;
	}

}
