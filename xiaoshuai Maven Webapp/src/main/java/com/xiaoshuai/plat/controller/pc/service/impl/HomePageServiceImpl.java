package com.xiaoshuai.plat.controller.pc.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiaoshuai.plat.base.BaseDao;
import com.xiaoshuai.plat.controller.pc.service.HomePageService;
import com.xiaoshuai.plat.pojo.Menu;
@Service("homePageService")
public class HomePageServiceImpl implements HomePageService{
	@Autowired
	private BaseDao baseDao;
	/**
	 * 查询所有的菜单
	 */
	@Override
	public List<Menu> getAllMenu() {
		String sql = "select * from menu ";
		 List<Map<String, Object>>  rows = this.baseDao.findByAll(sql);
		 List<Menu> list = new ArrayList<Menu>();;
		for (int i = 0; i < rows.size(); i++) {
			Menu menu = new Menu();
			if(rows.get(i).get("url")==null||rows.get(i).get("url").equals("")){
				menu.setUrl("");
			}else{
				menu.setUrl(rows.get(i).get("url").toString());
			}
			menu.setId(Integer.parseInt(rows.get(i).get("id").toString()));
			menu.setMenuname(rows.get(i).get("menuname").toString());
			menu.setParentid(Integer.parseInt(rows.get(i).get("parentid").toString()));
			list.add(menu);
		}
		return list;
	}



}
